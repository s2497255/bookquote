package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public HashMap<String,Double> bookPrice = new HashMap<String, Double>();
    double getBookPrice(String isbn){
        bookPrice.put("1",10.0);
        bookPrice.put("2",45.0);
        bookPrice.put("3",20.0);
        bookPrice.put("4",35.0);
        bookPrice.put("5",50.0);
        if (bookPrice.containsKey(isbn)) {
            return bookPrice.get(isbn);
        } else {
            //wrote a comment to check sourcetree
            return 0.0;
        }
    }
}
